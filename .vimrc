" General Settings ------------- {{{
set nocompatible  	"be iMproved
filetype off 		"required

"for power line
set laststatus=2
set encoding=utf-8

"define mapleader and maplocalleader
let mapleader = ","
let maplocalleader = "-"

" }}}

" Mappings ----------- {{{
" mapping move line up and down
noremap - ddp
noremap _ ddkP

" mapping space to insert space
nnoremap <space> i<space><esc>

" mappings to edit and source my vimrc file
nnoremap <leader>ev	:vsplit $MYVIMRC<cr>
nnoremap <leader>sv	:source $MYVIMRC<cr>

" uppercase a word 
inoremap <c-u> <esc>viwUea
nnoremap <c-u> viwU

" delete a line in insert mode
inoremap <c-d> <esc>ddi

"more advanced mappings
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel

"quoting a selection
vnoremap <leader>" <esc>`<i"<esc>`>a"<esc>

"the begining of the current line
nnoremap H V<esc>`<
nnoremap L V<esc>`>

"insert a line bellow
nnoremap J o<esc>
nnoremap K O<esc>

"canceling the arrow keys
inoremap <Right> <Nop>
inoremap <Left> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>

nnoremap <Right> <nop>
nnoremap <Left> <nop>
nnoremap <Up> <nop>
nnoremap <Down> <nop>

"nice escape
inoremap jk <esc>

"bye bye old escape
inoremap <esc> <nop>

" abbreviations
iabbrev @@ elaich.marouane@gmail.com

" }}}

"auto commands --------------------- {{{
"""""""""""""""""""""""""""""""""""""""""""

augroup numbering_code
	autocmd!
	autocmd Filetype python :setlocal number
	autocmd Filetype java :setlocal number
	autocmd Filetype php :setlocal number
augroup END

augroup indenting_php
   	autocmd!
	autocmd Filetype php :setlocal shiftwidth=3 softtabstop=3 noexpandtab smartindent
augroup END

augroup indenting_php
	autocmd!
	autocmd Filetype html :setlocal shiftwidth=2 softtabstop=2
augroup END

" Operator pending for markdown - current heading
augroup markdown_current_heading
	autocmd!
	autocmd Filetype markdown :onoremap ih :<c-u>execute "normal! ?\\(^==\\+$\\<Bar>^--\\+$\\)\r:nohlsearch\rkvg_"<cr>
augroup END

" comment this line 
augroup comment_line
	autocmd!
	autocmd Filetype python :nnoremap <localleader>c I#<esc>
	autocmd Filetype java :nnoremap <localleader>c I//<esc>
	autocmd Filetype php :nnoremap <localleader>c I//<esc>
augroup END


" java snippets
augroup java_snippets
	autocmd!
	autocmd Filetype java :iabbrev <buffer> rr return
	autocmd Filetype java :iabbrev <buffer> return NOPENOPENOPE

	autocmd Filetype java :iabbrev <buffer> puu public 
	autocmd Filetype java :iabbrev <buffer> public NOPENOPENOPE

	autocmd Filetype java :iabbrev <buffer> prr private
	autocmd Filetype java :iabbrev <buffer> private NOPENOPENOPE
augroup END

"Vimscript file settings ---------------- {{{
augroup filetype_vim
	autocmd!
	autocmd Filetype vim setlocal foldmethod=marker
augroup END
" }}}
"
" --------------------------------------- }}}


set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

 " let Vundle manage Vundle
 " required! 
Bundle 'gmarik/vundle'

Bundle 'http://github.com/scrooloose/nerdtree.git'
Bundle 'http://github.com/Lokaltog/vim-powerline.git'
Bundle 'http://github.com/tpope/vim-fugitive.git'

filetype plugin indent on	"required !

